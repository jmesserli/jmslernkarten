$(document).ready(function () {
    function pause(millis) {
        var date = new Date();
        var curDate = null;
        do {
            curDate = new Date();
        }
        while (curDate - date < millis)
    };

    $(".more").hide();
    $(".rnf").hide();
    var fW = "falsch";
    var rW = "richtig";
    $("body").css("display", "block");
    var r = 0;
    var f = 0;
    $("#Karten a").click(function () {
        $(this).parents("p").next(".more").slideDown();
        $(this).parents(".aroundquestion").find(".rnf").slideDown();
        $(this).html("");
    });

    $("p.ad").click(function () {
        alert("JM-Lernkaertchen v.1.0 *** www.jmnetwork.ch #JavaScript, PHP and MySQL - Joel Messerli 2013");
        $(this).hide("2000");
    });

    $("p.r").click(function () {
        r++;
        if (r > 1) {
            rW = "richtige";
        }
        $("p.aw").html(r + " " + rW + " und " + f + " " + fW + " (Fehlerquote: " + f/r +")");
        $(this).parent().prev().slideUp();
        $(this).parent().prev().prev().slideUp();
        $(this).parent().parent().find("hr").slideUp();
        $(this).parent().slideUp();
    });
    $("p.f").click(function () {
        f++;
        if (f > 1) {
            fW = "falsche";
        }
        $("p.aw").html(r + " " + rW + " und " + f + " " + fW + " (Fehlerquote: " + f/r +")");
        $(this).parent().prev().slideUp();
        $(this).parent().prev().prev().slideUp();
        $(this).parent().parent().find("hr").slideUp();
        $(this).parent().slideUp();
    });
});