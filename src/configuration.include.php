<?php
/*
 * This is the configuration file for JMLernkarten
 */


//Database hostname
$db_host = "127.0.0.1";

//MySQL User-name
$db_user = "root";

//Database password (User password)
$db_pass = "";

//Database name
$db_name = "";

//Site title (shown in Browser)
$site_name = "JM's Lernkarten - ".$znr;

/*
 * Configuration end
 */
?>
